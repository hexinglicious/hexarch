#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si --noconfirm

sudo pacman -Syyu --noconfirm
paru -Syyu --noconfirm

#Laptop with optimus
#paru -S --noconfirm optimus-manager
#sudo pacman -S --noconfirm nvidia-prime

sudo pacman -S --needed - < ./home/pkglist.txt

paru -S --needed - < ./home/foreignpkglist.txt

sudo systemctl enable ly.service
sudo systemctl enable betterlockscreen@$USER

/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
reboot
