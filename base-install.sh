#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Sofia /etc/localtime
hwclock --systohc
sed -i '161s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >> /etc/locale.conf
echo "hexArch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 hexArch.localdomain hexArch" >> /etc/hosts
#Change '123' to what you want for root
echo root:123 | chpasswd

#Programs
#Make sure to uncomment multilib in /etc/pacman.conf
#Set Parralel Downloads = CPU_THREADS + 2
#Uncomment Color flag

#AMD CPU
#pacman -S amd-ucode

#Intel CPU
#pacman -S intel-ucode

pacman -Syu && pacman -Syyu

pacman -S grub efibootmgr networkmanager dialog wpa_supplicant mtools dosfstools base-devel linux-headers avahi ntp xdg-utils bluez bluez-utils cups alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack wireplumber bash-completion reflector acpi bridge-utils acpid os-prober ntfs-3g zsh zsh-completions iw alacritty arandr e2fsprogs efivar htop imagemagick lib32-pipewire-jack numlockx xf86-input-libinput vulkan-icd-loader lib32-vulkan-icd-loader

#Drivers
#Don't forget to add the modules in /etc/mkinitcpio.conf

#nVidia GPU
#pacman -S --noconfirm nvidia nvidia-utils lib32-nvidia-utils nvidia-settings faudio lib32-faudio lib32-freetype2

#AMD GPU
#pacman -S --noconfirm xf86-video-amdgpu mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau

#Intel iGPU
#pacman -S --noconfirm lib32-mesa vulkan-intel lib32-vulkan-intel xf86-video-intel mesa libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi is you mounted the EFI partition at /boot/efi

grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager.service
systemctl enable bluetooth.service
systemctl enable reflector.timer

useradd -m hexing
#Change '123' to what you want for user(in this case hexing)
echo hexing:123 | chpasswd

echo "hexing ALL=(ALL) ALL" >> /etc/sudoers.d/hexing


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




