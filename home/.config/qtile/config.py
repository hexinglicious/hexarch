import os, subprocess, re

from libqtile import layout, hook
from libqtile.config import Match

from keys import keys, mod, terminal, mouse
from groups import groups
from screens import notifier, layouts, screens


widget_defaults = dict(
    font="Segoe UI Semibold",
    fontsize=11,
    padding=3,
)
extension_defaults = widget_defaults.copy()

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = False
wl_input_rules = None
wmname = "LG3D"

floating_layout = layout.Floating(
    float_rules=[
        Match(wm_class="connman-gtk"),
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(wm_class="Lxtask"),
        Match(wm_class="pavucontrol"),
        Match(wm_class="blueberry.py"),
        Match(wm_class="bitwarden"),
        Match(wm_class="file-roller"),
        Match(wm_class="crx_oeopbcgkkoapgobdbedcemjljbihmemj"), # Gmail pop-upxp
        Match(wm_class="net-pms-PMS"), # Universal Media Server
        Match(wm_class="qBittorrent"),
        Match(wm_class="arandr"),
        Match(wm_class="barrier"),
        Match(wm_class="nitrogen"),
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(title="Friends List"),
        Match(title="Rename File"),
        Match(title="Confirm File Replacing"),
        Match(title="Copying files"),
        Match(title="File Properties"),
        Match(title="Creating ..."),
        Match(title="Execute File"),
        Match(title="Creating New Folder"),
        Match(title="Error"),
        Match(title="Open Folder"),
        Match(title="Display Settings"),
        Match(title="Install Compatibility Tool"),
        Match(title="M&B II: Bannerlord"),
        Match(title="Picture-in-Picture"),
        Match(title="Save Image"),
        Match(title="Library"),
        Match(title="Add a new game"),
        Match(title=re.compile("Select*")),
        Match(title=re.compile("Properties*")),
        Match(title=re.compile("Configure*"))
    ],
    border_focus="#000000",
)

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.Popen([home + '/.config/qtile/autostart.sh'])