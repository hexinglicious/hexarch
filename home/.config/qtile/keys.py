from libqtile import layout
from libqtile.config import Click, Drag, Key
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"

terminal = guess_terminal()

keys = [
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "Tab", lazy.layout.next(), desc="Move window focus to next window"),
    Key([mod, "shift"], "Tab", lazy.next_screen(), desc="Move window focus to next screen"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "o", lazy.window.toggle_floating(), desc="Toggle floating window."),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen window."),
    # Sound
    Key([], "XF86AudioMute", lazy.spawn("amixer -D pulse sset Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -D pulse sset Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -D pulse sset Master 5%+")),
    # Player Controls
    Key([mod, "control"], "Up", lazy.spawn("playerctl play-pause")),
    Key([mod, "control"], "Right", lazy.spawn("playerctl next")),
    Key([mod, "control"], "Left", lazy.spawn("playerctl previous")),
    #Brightness Controls
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod, "control"], "Return", lazy.spawn("lxtask"), desc="Launch task manager"),
    Key([mod], "Space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, 'control'], "Space", lazy.spawn("vscodium /home/hexing/.config/qtile/config.py"), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "Delete", lazy.spawn("xkill"), desc="Kill a window."),
    #Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([], "Print", lazy.spawn("flameshot"), desc="Spawn flameshot"),
    Key([mod], "r", lazy.spawn("rofi -show combi"), desc="Spawn using an app using rofi"),
    Key([mod], "c", lazy.spawn("brave"), desc="Spawn brave browser."),
    Key([mod, "control"], "c", lazy.spawn("bitwarden-desktop"), desc="Spawn bitwarden."),
    Key([mod], "v", lazy.spawn("pcmanfm"), desc="Spawn pcmanfm."),
    Key([mod, "control"], "v", lazy.spawn("discord"), desc="Spawn discord."),
    Key([mod], "b", lazy.spawn("steam"), desc="Spawn steam."),
    Key([mod, "control"], "b", lazy.spawn("lutris"), desc="Spawn lutris."),
    Key([mod], "x", lazy.spawn("vscodium"), desc="Spawn vscodium."),
    Key([mod], "z", lazy.spawn("spotify"), desc="Spawn spotify."),
]

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]