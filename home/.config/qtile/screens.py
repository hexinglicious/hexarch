from libqtile import layout
from graphical_notifications import Notifier

from keys import terminal
from groups import groups
from platforms import num_screens, hostname
from bars import main_screen_laptop, main_screen_pc, side_screen

if num_screens[hostname] > 1:
    x = 2220
else:
    x = 1850
notifier = Notifier(x=x, 
                    y=30, 
                    width=300,
                    background='#1a1b1a',
                    border='#202020',
                    border_width=2,
                    corner_radius=8,
                    font_size=12,
                    max_windows=3,
                    overflow='more_height')

layouts = [
    layout.Columns(border_focus="#000000", border_normal="202020", border_width=2),
    layout.MonadTall(),
    layout.RatioTile(),
]

if num_screens[hostname] > 1:
    additional_screens = [side_screen] * num_screens[hostname]
    screens = [main_screen_pc]
    screens += additional_screens
else:
    screens = [main_screen_laptop]
