import platform


hostname = platform.node()

num_screens = {
    'hexArchLP': 1,
    'hexArchPC': 2
}
