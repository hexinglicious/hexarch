from libqtile.config import Group, Key
from libqtile.lazy import lazy

from keys import keys, mod
from platforms import num_screens, hostname


if num_screens[hostname] > 1:
    groups = [
        Group("1", label="Ⅰ"),
        Group("2", label="Ⅱ"),
        Group("3", label="Ⅲ"),
        Group("4", label="Ⅳ"),
        Group("5", label="Ⅴ"),
        Group("6", label="Ⅵ"),
        Group("7", label="Ⅶ"),
        Group("8", label="Ⅷ"),
        Group("9", label="Ⅸ"),
    ]
    groups = groups[:num_screens[hostname]*3]
else:
    groups = [
        Group("1", label="Ⅰ"),
        Group("2", label="Ⅱ"),
        Group("3", label="Ⅲ"),
        Group("4", label="Ⅳ"),
        # Group("1", label="(¬‿¬)"),
        # Group("2", label="(  ͡•_ ͡•)"),
        # Group("3", label=" ʕ•ᴥ•ʔ "),
        # Group("4", label="( ͡° ͜ʖ ͡°)"),
    ]


# Disallow swapping groups
def switch_screens(index: int, name: str):
    @lazy.function
    def __inner(qtile, index=index, name=name):
        i = qtile.screens.index(qtile.current_screen)
        group = qtile.screens[i - 1].group
        nextgroup = qtile.groups_map[groups[index].name]
        if group != nextgroup:
            qtile.current_screen.set_group(nextgroup)
    return __inner

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                switch_screens(int(i.name) - 1, i.name),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )