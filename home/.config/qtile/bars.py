from libqtile import bar, widget
from libqtile.config import Screen
from libqtile.lazy import lazy

from keys import terminal


main_screen_laptop = Screen(
    top=bar.Bar(
        [
            widget.Image(
                filename='~/.config/qtile/icons/arch2.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('rofi -show combi'),
                                 'Button3': lazy.reload_config()}
            ),
            widget.GroupBox(
                highlight_color=['00000000', '00000000'],
                active='ffffff',
                inactive='909090',
                highlight_method='line',
                disable_drag=True,
                margin_y=4),
            widget.WindowName(),
            widget.Spacer(),
            widget.Clock(format="%A   %d %B %Y   %R"),
            widget.Spacer(),
            widget.Systray(background="#00000000", 
                            icon_size=14,
                            padding=5),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/cpu.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.CPU(
                format='{load_percent}% ',
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Spacer(length=1),
            widget.Image(
                filename='~/.config/qtile/icons/mem.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Memory(
                format='{MemUsed:.0f}{mm}B ',
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Image(
                filename='~/.config/qtile/icons/audio-volume-high.svg',
                margin=2,
                mouse_callbacks={'Button1': lazy.spawn('amixer -D pulse sset Master toggle'),
                                    'Button2': lazy.spawn('pavucontrol')}),
            widget.Volume(
                fmt='{}',
                padding=2,
                update_interval=0.1,
                mouse_callbacks={'Button2': lazy.spawn('pavucontrol')},
                device='pulse',
                volume_app='pavucontrol',
                volume_up_command=lazy.spawn('amixer -D pulse sset Master 5%+'),
                volume_down_command=lazy.spawn('amixer -D pulse sset Master 5%-'),),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/wireless_3.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn(terminal + ' -e nmtui')}),
            widget.Wlan(
                interface='wlo1', 
                format='{percent:2.0%}',
                mouse_callbacks={'Button1': lazy.spawn(terminal + ' -e nmtui')}),
            widget.Spacer(length=5),
            widget.BatteryIcon(theme_path='~/.config/qtile/icons', scale=1),
            widget.Battery(format='{percent:2.0%}', 
                notify_below=15,
                hide_threshold=0.99,
                mouse_callbacks={'Button1': lazy.spawn('~/.config/qtile/battery.sh')}),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/power_w.svg',
                margin=1,
            mouse_callbacks={'Button1': lazy.spawn('/home/hexing/.config/rofi/scripts/powermenu_t1')}),
        ],
        18,
        background="#00000000",
    ),
)

main_screen_pc = Screen(
    top=bar.Bar(
        [
            widget.Image(
                filename='~/.config/qtile/icons/arch2.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('rofi -show combi'),
                                 'Button3': lazy.reload_config()}
            ),
            widget.GroupBox(
                highlight_color=['00000000', '00000000'],
                active='ffffff',
                inactive='909090',
                highlight_method='line',
                disable_drag=True,
                margin_y=4),
            widget.WindowName(),
            widget.Spacer(),
            widget.Clock(format="%A   %d %B %Y   %R"),
            widget.Spacer(),
            widget.Systray(background="#00000000", 
                            icon_size=14,
                            padding=5),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/cpu.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.CPU(
                format='{load_percent}% ',
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Spacer(length=1),
            widget.Image(
                filename='~/.config/qtile/icons/mem.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Memory(
                format='{MemUsed:.0f}{mm}B ',
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Image(
                filename='~/.config/qtile/icons/audio-volume-high.svg',
                margin=2,
                mouse_callbacks={'Button1': lazy.spawn('amixer -D pulse sset Master toggle'),
                                    'Button2': lazy.spawn('pavucontrol')}),
            widget.Volume(
                fmt='{}',
                padding=2,
                update_interval=0.1,
                mouse_callbacks={'Button2': lazy.spawn('pavucontrol')},
                device='pulse',
                volume_app='pavucontrol',
                volume_up_command=lazy.spawn('amixer -D pulse sset Master 5%+'),
                volume_down_command=lazy.spawn('amixer -D pulse sset Master 5%-'),),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/wired.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn(terminal + ' -e nmtui')}),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/power_w.svg',
                margin=1,
            mouse_callbacks={'Button1': lazy.spawn('/home/hexing/.config/rofi/scripts/powermenu_t1')}),
        ],
        18,
        background="#00000000",
    ),
)

side_screen = Screen(
    top=bar.Bar(
        [
            widget.Image(
                filename='~/.config/qtile/icons/arch2.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('rofi -show combi'),
                                 'Button3': lazy.reload_config()}
            ),
            widget.GroupBox(
                highlight_color=['00000000', '00000000'],
                active='ffffff',
                inactive='909090',
                highlight_method='line',
                disable_drag=True,
                margin_y=4),
            widget.WindowName(),
            widget.Spacer(),
            widget.Clock(format="%A   %d %B %Y   %R"),
            widget.Spacer(),
            widget.Image(
                filename='~/.config/qtile/icons/cpu.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.CPU(
                format='{load_percent}% ',
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Spacer(length=1),
            widget.Image(
                filename='~/.config/qtile/icons/mem.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Memory(
                format='{MemUsed:.0f}{mm}B ',
                mouse_callbacks={'Button1': lazy.spawn('lxtask')}),
            widget.Image(
                filename='~/.config/qtile/icons/audio-volume-high.svg',
                margin=2,
                mouse_callbacks={'Button1': lazy.spawn('amixer -D pulse sset Master toggle'),
                                    'Button2': lazy.spawn('pavucontrol')}),
            widget.Volume(
                fmt='{}',
                padding=2,
                update_interval=0.1,
                mouse_callbacks={'Button2': lazy.spawn('pavucontrol')},
                device='pulse',
                volume_app='pavucontrol',
                volume_up_command=lazy.spawn('amixer -D pulse sset Master 5%+'),
                volume_down_command=lazy.spawn('amixer -D pulse sset Master 5%-')),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/wired.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn(terminal + ' -e nmtui')}),
            widget.Spacer(length=5),
            widget.Image(
                filename='~/.config/qtile/icons/power_w.svg',
                margin=1,
                mouse_callbacks={'Button1': lazy.spawn('/home/hexing/.config/rofi/scripts/powermenu_t1')}),
        ],
        18,
        background="#00000000",
    ),
)

 