#!/bin/bash

sudo sed -i 's/startup_mode=nvidia/startup_mode=hybrid/' /etc/optimus-manager/optimus-manager.conf
sudo sed -i 's/startup_mode=integrated/startup_mode=hybrid/' /etc/optimus-manager/optimus-manager.conf
sleep 1
reboot