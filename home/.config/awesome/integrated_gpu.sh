#!/bin/bash

sudo sed -i 's/startup_mode=nvidia/startup_mode=integrated/' /etc/optimus-manager/optimus-manager.conf
sudo sed -i 's/startup_mode=hybrid/startup_mode=integrated/' /etc/optimus-manager/optimus-manager.conf
sleep 1
reboot