 local menu98edb85b00d9527ad5acebe451b3fae6 = {
     {"Archive Manager", "file-roller "},
     {"Barrier", "barrier"},
     {"Bitwarden", "bitwarden-desktop", "/usr/share/icons/hicolor/16x16/apps/bitwarden.png" },
     {"Disks", "gnome-disks"},
     {"EmuSAK", "emusak ", "/usr/share/pixmaps/emusak.png" },
     {"Galculator", "galculator", "/usr/share/icons/hicolor/48x48/apps/galculator.png" },
     {"Image Viewer", "gpicview ", "/usr/share/icons/hicolor/48x48/apps/gpicview.png" },
     {"Oversteer", "/usr/bin/oversteer"},
     {"ProtonUp-Qt", "net.davidotek.pupgui2", "/usr/share/icons/hicolor/64x64/apps/net.davidotek.pupgui2.png" },
     {"Protontricks", "protontricks --no-term --gui"},
     {"VSCodium", "/opt/vscodium-bin/bin/codium --no-sandbox --unity-launch ", "/usr/share/pixmaps/vscodium.png" },
     {"Vim", "xterm -e vim ", "/usr/share/icons/hicolor/48x48/apps/gvim.png" },
     {"Winetricks", "winetricks --gui"},
     {"nitrogen", "nitrogen", "/usr/share/icons/hicolor/16x16/apps/nitrogen.png" },
     {"picom", "picom"},
 }

 local menu251bd8143891238ecedc306508e29017 = {
     {"Dosbox", "dosbox", "/usr/share/pixmaps/dosbox.png" },
     {"Heroic Games Launcher", "/opt/Heroic/heroic ", "/usr/share/icons/hicolor/16x16/apps/heroic.png" },
     {"Lutris", "lutris ", "/usr/share/icons/hicolor/16x16/apps/lutris.png" },
     {"MultiMC", "multimc", "/usr/share/pixmaps/multimc.svg" },
     {"Oversteer", "/usr/bin/oversteer"},
     {"ProtonUp-Qt", "net.davidotek.pupgui2", "/usr/share/icons/hicolor/64x64/apps/net.davidotek.pupgui2.png" },
     {"Steam (Native)", "/usr/bin/steam-native ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Steam (Runtime)", "/usr/bin/steam-runtime ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"yuzu", "yuzu "},
 }

 local menud334dfcea59127bedfcdbe0a3ee7f494 = {
     {"Flameshot", "/usr/bin/flameshot", "/usr/share/icons/hicolor/48x48/apps/org.flameshot.Flameshot.png" },
     {"GNU Image Manipulation Program", "gimp-2.10 ", "/usr/share/icons/hicolor/16x16/apps/gimp.png" },
     {"Image Viewer", "gpicview ", "/usr/share/icons/hicolor/48x48/apps/gpicview.png" },
     {"Inkscape", "inkscape ", "/usr/share/icons/hicolor/16x16/apps/org.inkscape.Inkscape.png" },
 }

 local menuc8205c7636e728d448c2774e6a4a944b = {
     {"Avahi SSH Server Browser", "/usr/bin/bssh"},
     {"Avahi VNC Server Browser", "/usr/bin/bvnc"},
     {"Brave", "brave ", "/usr/share/icons/hicolor/16x16/apps/brave-desktop.png" },
     {"Discord", "/usr/bin/discord", "/usr/share/pixmaps/discord.png" },
     {"Microsoft Teams - Preview", "teams ", "/usr/share/pixmaps/teams.png" },
     {"Slack", "/usr/bin/slack -s ", "/usr/share/pixmaps/slack.png" },
     {"Steam (Native)", "/usr/bin/steam-native ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Steam (Runtime)", "/usr/bin/steam-runtime ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Zoom", "/usr/bin/zoom ", "/usr/share/pixmaps/Zoom.png" },
     {"qBittorrent", "qbittorrent ", "/usr/share/icons/hicolor/16x16/apps/qbittorrent.png" },
 }

 local menudf814135652a5a308fea15bff37ea284 = {
     {"ONLYOFFICE Desktop Editors", "/usr/bin/onlyoffice-desktopeditors ", "/usr/share/icons/hicolor/16x16/apps/onlyoffice-desktopeditors.png" },
     {"qpdfview", "qpdfview --unique "},
 }

 local menue6f43c40ab1c07cd29e4e83e4ef6bf85 = {
     {"Electron 19", "electron19 ", "/usr/share/pixmaps/electron19.png" },
     {"VSCodium", "/opt/vscodium-bin/bin/codium --no-sandbox --unity-launch ", "/usr/share/pixmaps/vscodium.png" },
 }

 local menu52dd1c847264a75f400961bfb4d1c849 = {
     {"PulseAudio Volume Control", "pavucontrol"},
     {"Qt V4L2 test Utility", "qv4l2", "/usr/share/icons/hicolor/16x16/apps/qv4l2.png" },
     {"Qt V4L2 video capture utility", "qvidcap", "/usr/share/icons/hicolor/16x16/apps/qvidcap.png" },
     {"Spotify", "spotify --uri=", "/usr/share/pixmaps/spotify-client.png" },
     {"Universal Media Server", "/opt/ums/UMS.sh", "/usr/share/pixmaps/ums.png" },
     {"VLC media player", "/usr/bin/vlc --started-from-file ", "/usr/share/icons/hicolor/16x16/apps/vlc.png" },
 }

 local menuee69799670a33f75d45c57d1d1cd0ab3 = {
     {"Alacritty", "alacritty", "/usr/share/pixmaps/Alacritty.svg" },
     {"Avahi Zeroconf Browser", "/usr/bin/avahi-discover"},
     {"File Manager PCManFM", "pcmanfm "},
     {"Htop", "xterm -e htop", "/usr/share/pixmaps/htop.png" },
     {"Manage Printing", "/usr/bin/xdg-open http://localhost:631/", "/usr/share/icons/hicolor/16x16/apps/cups.png" },
     {"OpenJDK Java 11 Console", "/usr/lib/jvm/java-11-openjdk/bin/jconsole", "/usr/share/icons/hicolor/16x16/apps/java11-openjdk.png" },
     {"OpenJDK Java 11 Shell", "xterm -e /usr/lib/jvm/java-11-openjdk/bin/jshell", "/usr/share/icons/hicolor/16x16/apps/java11-openjdk.png" },
     {"Task Manager", "lxtask"},
     {"UXTerm", "uxterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"XTerm", "xterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
 }

xdgmenu = {
    {"Accessories", menu98edb85b00d9527ad5acebe451b3fae6},
    {"Games", menu251bd8143891238ecedc306508e29017},
    {"Graphics", menud334dfcea59127bedfcdbe0a3ee7f494},
    {"Internet", menuc8205c7636e728d448c2774e6a4a944b},
    {"Office", menudf814135652a5a308fea15bff37ea284},
    {"Programming", menue6f43c40ab1c07cd29e4e83e4ef6bf85},
    {"Sound & Video", menu52dd1c847264a75f400961bfb4d1c849},
    {"System Tools", menuee69799670a33f75d45c57d1d1cd0ab3},
}

