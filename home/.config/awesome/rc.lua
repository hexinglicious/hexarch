pcall(require, "luarocks.loader")
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
local lain = require("lain")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
require("awful.hotkeys_popup.keys")
xdg_menu = require("archmenu")

--Awesome Widgets definitions
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
local logout_menu_widget = require("awesome-wm-widgets.logout-menu-widget.logout-menu")
local calendar_widget = require("awesome-wm-widgets.calendar-widget.calendar")
local battery_widget = require("awesome-wm-widgets.battery-widget.battery")
local net_widgets = require("net_widgets")

-- Error handling
-- Check if awesome encountered an error during startup and fall back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

beautiful.init(gears.filesystem.get_configuration_dir() .. "/themes/default/theme.lua")
local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), "default")
beautiful.init(theme_path)

terminal = "alacritty"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor


modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.fair,
    awful.layout.suit.floating
}

-- Create a launcher widget and a main menu
myawesomemenu = {
   { "Hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "Manual", terminal .. " -e man awesome" },
   { "Edit Config", "vscodium" .. " " .. awesome.conffile },
   { "Restart", awesome.restart },
   { "Quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({ items = { { "Awesome", myawesomemenu },
                                    { "Applications", xdgmenu },
                                    { "Open Terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ 
    image = gears.filesystem.get_configuration_dir() .. "./themes/default/icons/arch2.svg",
    menu = mymainmenu
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- Create a textclock widget
mytextclock = wibox.widget.textclock('%A  %B %e  %R')
local cw = calendar_widget({
    theme = 'dark',
    placement = beautiful.bar_position,
    start_sunday = false,
    radius = 4,
    previous_month_button = 3,
    next_month_button = 1,
})
mytextclock:connect_signal("button::press",
    function(_, _, _, button)
        if button == 1 then cw.toggle() end
    end)

-- Spacers and Separators
separator = wibox.widget.textbox("|")
spacer = wibox.widget.textbox(" ")
spacerx2 = wibox.widget.textbox("  ")
spacerx3 = wibox.widget.textbox("   ")

-- Net Widget
net_wireless = net_widgets.wireless({
    interface = "wlo1", 
    onclick = terminal .. " -e nmtui", 
    popup_position=beautiful.bar_position.."_right"})

net_wired = net_widgets.indicator({
    interfaces = {"enp3s0"}, 
    onclick = terminal .. " -e nmtui", 
    popup_position=beautiful.bar_position.."_right"})

local markup = lain.util.markup
local memicon = wibox.widget.imagebox(beautiful.widget_mem)
local mem = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.font(beautiful.font , " " .. mem_now.used .. "MB "))
    end
})
local cpuicon = wibox.widget.imagebox(beautiful.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.font(beautiful.font, " " .. cpu_now.usage .. "% "))
    end
})

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
     -- Each screen has its own tag table.
     awful.tag({ "(¬‿¬)", "(  ͡•_ ͡•)", " ʕ•ᴥ•ʔ ", "( ͡° ͜ʖ ͡°)" }, s, awful.layout.layouts[1])

     -- Create a promptbox for each screen
     s.mypromptbox = awful.widget.prompt()

     -- Create an imagebox widget which will contain an icon indicating which layout we're using.
     -- We need one layoutbox per screen.
     s.mylayoutbox = awful.widget.layoutbox(s)
     s.mylayoutbox:buttons(gears.table.join(
                            awful.button({ }, 1, function () awful.layout.inc( 1) end),
                            awful.button({ }, 3, function () awful.layout.inc(-1) end),
                            awful.button({ }, 4, function () awful.layout.inc( 1) end),
                            awful.button({ }, 5, function () awful.layout.inc(-1) end)))

     -- Create a taglist widget
     s.mytaglist = awful.widget.taglist {
         screen  = s,
         filter  = awful.widget.taglist.filter.all,
         buttons = taglist_buttons,
     }

     -- Create a tasklist widget
     s.mytasklist = awful.widget.tasklist {
         screen  = s,
         filter  = awful.widget.tasklist.filter.currenttags,
         buttons = tasklist_buttons
     }

     -- Create the wibox
     s.mywibox = awful.wibar({ position = beautiful.bar_position, screen = s, height = beautiful.bar_height })

     -- Add widgets to the wibox
     s.mywibox:setup {
         layout = wibox.layout.stack,
         {
             layout = wibox.layout.align.horizontal,
             { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                {
                    mylauncher,
                    margins = 2,
                    layout = wibox.container.margin
                },
                spacer,
                s.mytaglist,
                s.mypromptbox,
                --s.mylayoutbox,
             },
            --  s.mytasklist, -- Middle widget
             nil,
             { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                -- mykeyboardlayout,
                spacerx2,
                {
                    wibox.widget.systray(),
                    top = 2,
                    left = 0,
                    bottom = 2,
                    right = 0,
                    layout = wibox.container.margin
                },
                spacerx2,
                --  wibox.widget.textbox("CPU:"),
                {
                    cpuicon,
                    top = 2,
                    left = 0,
                    bottom = 2,
                    right = 0,
                    layout = wibox.container.margin
                },
                {
                    cpu,
                    top = 1,
                    left = 0,
                    bottom = 2,
                    right = 0,
                    layout = wibox.container.margin
                },
                spacerx2,
                {
                    memicon,
                    top = 1,
                    left = 0,
                    bottom = 3,
                    right = 0,
                    layout = wibox.container.margin
                },
                --  wibox.widget.textbox("RAM:"),
                {
                    mem,
                    top = 1,
                    left = 0,
                    bottom = 2,
                    right = 0,
                    layout = wibox.container.margin
                },
                spacerx2,
                -- battery_widget{
                --     notification_position = beautiful.bar_position.."_right", 
                --     warning_msg_position = beautiful.bar_position.."_right"
                -- },
                -- spacerx3,
                volume_widget{
                    mixer_cmd = 'pavucontrol',
                    widget_type = 'icon-and-text',
                },
                spacerx2,
                {
                    net_wired,
                    top = 2,
                    left = 0,
                    bottom = 2,
                    right = 0,
                    layout = wibox.container.margin
                },
                spacer,
                logout_menu_widget{
                    font = beautiful.font
                },
             },
         },
         {
             mytextclock,
             valign = "center",
             halign = "center",
             layout = wibox.container.place
         }
     }
 end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),


    -- Window focus manipulation by direction
    awful.key({ modkey,    }, "h", function () 
            awful.client.focus.global_bydirection("left")
        end,
        {description = "focus next window left", group = "client"}),
    awful.key({ modkey,    }, "j", function () 
            awful.client.focus.global_bydirection("down")
        end,
        {description = "focus next window down", group = "client"}),
    awful.key({ modkey,    }, "l", function () 
            awful.client.focus.global_bydirection("right")
        end,
        {description = "focus next window right", group = "client"}),
    awful.key({ modkey,    }, "k", function () 
            awful.client.focus.global_bydirection("up")
        end,
        {description = "focus next window up", group = "client"}),


    -- Window layout manipulation by direction
    awful.key({ modkey, "Shift"   }, "h", function () 
            awful.client.swap.global_bydirection("left")
        end,
        {description = "swap with left client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "j", function () 
            awful.client.swap.global_bydirection("down")
        end,
        {description = "swap with down client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "l", function () 
            awful.client.swap.global_bydirection("right")
        end,
        {description = "swap with right client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () 
            awful.client.swap.global_bydirection("up")
        end,
        {description = "swap with up client", group = "client"}),
    

    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    awful.key({ modkey,           }, "Tab", function () awful.client.focus.byidx(1) end,
              {description = "focus next window", group = "client"}),

    awful.key({ modkey,    "Control" }, "Tab", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),          


    -- Manipulate and select windows
    awful.key({ modkey,   "Control"        }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,   "Control"       }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),

    awful.key({ modkey, "Control"   }, "k",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control"   }, "j",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),

    awful.key({ modkey, "Mod1" }, "k",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Mod1" }, "j",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),

    -- Swap layout
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    --Volume Controls
        -- Volume Keys
    awful.key({}, "XF86AudioLowerVolume", function ()
        volume_widget:dec(5) end),
    awful.key({}, "XF86AudioRaiseVolume", function ()
        volume_widget:inc(5) end),
    awful.key({}, "XF86AudioMute", function ()
        volume_widget:toggle() end),


    -- Media Keys
    awful.key({ modkey, "Control" }, "Up", function()
        awful.util.spawn("playerctl play-pause", false) end),
    awful.key({ modkey, "Control"  }, "Right", function()
        awful.util.spawn("playerctl next", false) end),
    awful.key({ modkey, "Control"  }, "Left", function()
        awful.util.spawn("playerctl previous", false) end),


    --Brightness Controls
    awful.key({}, "XF86MonBrightnessUp", function ()
        awful.util.spawn("xbacklight -inc 5", false) end),
    awful.key({}, "XF86MonBrightnessDown", function ()
        awful.util.spawn("xbacklight -dec 5", false) end),

    -- Standard programs
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
        {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
        {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Control"   }, "Esc", awesome.quit,
             {description = "quit awesome", group = "awesome"}),
              
    -- Take Screenshot
    awful.key({ }, "Print", function () awful.util.spawn("flameshot", false) end),

    -- Show Task Manager
    awful.key({ modkey, "Control" },    "Return",     function () 
        awful.util.spawn("lxtask") end,
                {description = "show task manager", group = "launcher"}),

    -- Kill Task
    awful.key({ modkey, "Control" },    "Delete",     function () 
        awful.util.spawn("xkill") end,
                {description = "kill task", group = "launcher"}),        
    
    -- Edit Config
    awful.key({ modkey, "Control" },    "space",     function () 
        awful.util.spawn("vscodium /home/hexing/.config/awesome/rc.lua") end,
                {description = "edit config", group = "launcher"}),  

    -- Edit Theme Config
    awful.key({ modkey, "Mod1" },    "space",     function () 
        awful.util.spawn("vscodium /home/hexing/.config/awesome/themes/default/theme.lua") end,
                {description = "edit theme config", group = "launcher"}),  

    -- Prompt Run
    awful.key({ modkey },    "r",     function () 
        awful.util.spawn("rofi -show combi") end,
              {description = "run rofi in run mode", group = "launcher"}),

    -- Brave
    awful.key({ modkey },     "c",     function () 
        awful.util.spawn("brave") end,
              {description = "launch brave", group = "application"}),

    -- Pcmanfm
    awful.key({ modkey },     "v",     function () 
        awful.util.spawn("pcmanfm") end,
              {description = "launch pcmanfm", group = "application"}),

    -- Steam
    awful.key({ modkey },     "b",     function () 
        awful.util.spawn("steam") end,
              {description = "launch steam", group = "application"}),

    -- VSCodium
    awful.key({ modkey },     "x",     function () 
        awful.util.spawn("vscodium") end,
              {description = "launch VSCodium", group = "application"}),

    -- Discord
    awful.key({ modkey },     "z",     function () 
        awful.util.spawn("spotify") end,
              {description = "launch spotify", group = "application"}),
    
    -- Bitwarden
    awful.key({ modkey, "Control" },     "c",     function () 
        awful.util.spawn("bitwarden-desktop") end,
                {description = "launch bitwarden", group = "application"}),

    -- Discord
    awful.key({ modkey, "Control" },     "v",     function () 
        awful.util.spawn("discord") end,
                {description = "launch discord", group = "application"}),

    -- Lutris
    awful.key({ modkey, "Control" },     "b",     function () 
        awful.util.spawn("lutris") end,
                {description = "launch lutris", group = "application"})
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey,       }, "q",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,  "Shift"         }, "Tab",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    -- awful.key({ modkey,     "Control"      }, "n",
    --     function (c)
    --         -- The client currently has the input focus, so it cannot be
    --         -- minimized, since minimized clients can't have the focus.
    --         c.minimized = true
    --     end ,
    --     {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    -- awful.key({ modkey, "Control" }, "m",
    --     function (c)
    --         c.maximized_vertical = not c.maximized_vertical
    --         c:raise()
    --     end ,
    -- {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer",
          "Pavucontrol",
          "Blueberry.py",
          "PeaZip",
          "Galculator",
          "Lxtask",
          "net-pms-PMS",
          "Barrier",
          "Bitwarden",
          "File-roller",
          "Connman-gtk",
          "Popsicle"
        },
        name = {
          "Event Tester",   -- xev.
          "Friends List",   -- Steam friends list
          "Copying files",
          "File Properties"
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true, 
                        ontop = true,
                        placement = awful.placement.centered,
                         }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },
}

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
   c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

--Rounded corners for floating windows
client.connect_signal("property::floating", function (c)
    if c.floating then
        c.shape = function(cr,w,h)
          gears.shape.rounded_rect(cr,w,h,4)
        end
      else
        c.shape = gears.shape.rectangle
      end  
end)

--Disallow minimization.
client.connect_signal("property::minimized", function(c)
	    c.minimized = false
    end)
-- }}}

-- Run garbage collector regularly to prevent memory leaks
gears.timer {
    timeout = 30,
    autostart = true,
    callback = function() collectgarbage() end
}

--Autostart Applications
awful.spawn.with_shell("nitrogen --restore")
awful.spawn.with_shell("xdg_menu --format awesome --root-menu /etc/xdg/menus/arch-applications.menu | awk -F, '{if (a!=$1) print $a; a=$1}' >~/.config/awesome/archmenu.lua")