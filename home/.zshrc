# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/hexing/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstal

# Git config
function parse_git_branch() {
	    git branch 2> /dev/null | sed -n -e 's/^\* \(.*\)/[\1]/p'
    }

COLOR_DEF=$'\e[0m'
COLOR_USR=$'\e[38;5;243m'
COLOR_DIR=$'\e[38;5;197m'
COLOR_GIT=$'\e[38;5;39m'
setopt PROMPT_SUBST

# Prompt config
autoload -Uz promptinit
promptinit
PROMPT='%F{#ff6047}%n%f%F{#27b2d9}@%f%F{#49c978}%m%f %{%F{197}%}%~%{%F{39}%}$(parse_git_branch)%{%F{normal}%} '

#Aliases
alias ll="ls -la --color"
alias update="paru"
alias clear-cache="sudo pacman -Sc --noconfirm"
alias remove-orphans="sudo pacman -Qtdq | sudo pacman -Rns - && yay -Qtdq | yay -Rns -"
alias dump-installed-pkgs="pacman -Qqen > ./pkglist.txt && comm -13 <(pacman -Qqdt | sort) <(pacman -Qqdtt | sort) > ./optdeplist.txt && pacman -Qqem > ./foreignpkglist.txt"
alias install-from-list="sudo pacman -S --needed - <"
alias paru-install-from-list="paru -S --needed - <"
alias regen-initramfs="sudo mkinitcpio -P"
alias grub-update="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias sync-clock="sudo ntpd -qg && sudo hwclock --systohc"
alias check-gpu="glxinfo | grep OpenGL"
alias pacman-fix-lock="sudo rm /var/lib/pacman/db.lck"
alias systemd-enabled-services="systemctl list-unit-files | grep enable"
alias systemd-running-services="systemctl | grep running"

#KeyBindings
# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"
key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete
[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
 	function zle_application_mode_stop { echoti rmkx }
 	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
 	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
